FROM python:3.11-alpine3.18
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update
RUN apk add gcc 
RUN apk add libpq-dev
RUN apk add postgresql-dev
RUN apk add build-base


WORKDIR /usr/src/chitchat
COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /usr/src/chitchat/backend
