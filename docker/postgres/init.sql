--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Debian 12.8-1.pgdg100+1)
-- Dumped by pg_dump version 12.8 (Debian 12.8-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: chatauth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chatauth_user (
    id bigint NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    username character varying(150) NOT NULL,
    room character varying(256) NOT NULL,
    logo character varying(100)
);


ALTER TABLE public.chatauth_user OWNER TO postgres;

--
-- Name: chatauth_user_friends; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chatauth_user_friends (
    id bigint NOT NULL,
    from_user_id bigint NOT NULL,
    to_user_id bigint NOT NULL
);


ALTER TABLE public.chatauth_user_friends OWNER TO postgres;

--
-- Name: chatauth_user_friends_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chatauth_user_friends_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chatauth_user_friends_id_seq OWNER TO postgres;

--
-- Name: chatauth_user_friends_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chatauth_user_friends_id_seq OWNED BY public.chatauth_user_friends.id;


--
-- Name: chatauth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chatauth_user_groups (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.chatauth_user_groups OWNER TO postgres;

--
-- Name: chatauth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chatauth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chatauth_user_groups_id_seq OWNER TO postgres;

--
-- Name: chatauth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chatauth_user_groups_id_seq OWNED BY public.chatauth_user_groups.id;


--
-- Name: chatauth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chatauth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chatauth_user_id_seq OWNER TO postgres;

--
-- Name: chatauth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chatauth_user_id_seq OWNED BY public.chatauth_user.id;


--
-- Name: chatauth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chatauth_user_user_permissions (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.chatauth_user_user_permissions OWNER TO postgres;

--
-- Name: chatauth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chatauth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chatauth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: chatauth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chatauth_user_user_permissions_id_seq OWNED BY public.chatauth_user_user_permissions.id;


--
-- Name: chitchat_chat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chitchat_chat (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    chat_type character varying(64) NOT NULL,
    logo character varying(100),
    owner_id bigint
);


ALTER TABLE public.chitchat_chat OWNER TO postgres;

--
-- Name: chitchat_chat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chitchat_chat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chitchat_chat_id_seq OWNER TO postgres;

--
-- Name: chitchat_chat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chitchat_chat_id_seq OWNED BY public.chitchat_chat.id;


--
-- Name: chitchat_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chitchat_message (
    id bigint NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    timeupdate timestamp with time zone NOT NULL,
    text text NOT NULL,
    author_id bigint,
    chat_id bigint NOT NULL,
    edited boolean NOT NULL,
    meta jsonb NOT NULL,
    read boolean NOT NULL,
    reply_to_id bigint,
    sent boolean NOT NULL
);


ALTER TABLE public.chitchat_message OWNER TO postgres;

--
-- Name: chitchat_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chitchat_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chitchat_message_id_seq OWNER TO postgres;

--
-- Name: chitchat_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chitchat_message_id_seq OWNED BY public.chitchat_message.id;


--
-- Name: chitchat_userchat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chitchat_userchat (
    id bigint NOT NULL,
    chat_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.chitchat_userchat OWNER TO postgres;

--
-- Name: chitchat_userchat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chitchat_userchat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chitchat_userchat_id_seq OWNER TO postgres;

--
-- Name: chitchat_userchat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chitchat_userchat_id_seq OWNED BY public.chitchat_userchat.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id bigint NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: chatauth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user ALTER COLUMN id SET DEFAULT nextval('public.chatauth_user_id_seq'::regclass);


--
-- Name: chatauth_user_friends id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_friends ALTER COLUMN id SET DEFAULT nextval('public.chatauth_user_friends_id_seq'::regclass);


--
-- Name: chatauth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.chatauth_user_groups_id_seq'::regclass);


--
-- Name: chatauth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.chatauth_user_user_permissions_id_seq'::regclass);


--
-- Name: chitchat_chat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_chat ALTER COLUMN id SET DEFAULT nextval('public.chitchat_chat_id_seq'::regclass);


--
-- Name: chitchat_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_message ALTER COLUMN id SET DEFAULT nextval('public.chitchat_message_id_seq'::regclass);


--
-- Name: chitchat_userchat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_userchat ALTER COLUMN id SET DEFAULT nextval('public.chitchat_userchat_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add Чат	6	add_chat
22	Can change Чат	6	change_chat
23	Can delete Чат	6	delete_chat
24	Can view Чат	6	view_chat
25	Can add Комнаты пользователей	7	add_userchat
26	Can change Комнаты пользователей	7	change_userchat
27	Can delete Комнаты пользователей	7	delete_userchat
28	Can view Комнаты пользователей	7	view_userchat
29	Can add сообщение	8	add_message
30	Can change сообщение	8	change_message
31	Can delete сообщение	8	delete_message
32	Can view сообщение	8	view_message
33	Can add user	9	add_user
34	Can change user	9	change_user
35	Can delete user	9	delete_user
36	Can view user	9	view_user
\.


--
-- Data for Name: chatauth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chatauth_user (id, password, last_login, is_superuser, first_name, last_name, is_staff, is_active, date_joined, email, username, room, logo) FROM stdin;
9	pbkdf2_sha256$260000$SI59qCHu2yIrQtj8bz8DpJ$xzhrMx8s7y9bvzBwc3lYyqn2EdHr20V5tTBA49A8YfQ=	2022-03-15 17:04:41+00	f			f	t	2022-03-15 17:04:29+00		alena	e82120a163e848d6b7cafd6331c9bdf7	user/avatar/IMG_20230323_004414_809.jpg
7	pbkdf2_sha256$260000$Mp8cBuLCoqTT6Bg0ZuUMik$envvlM62n5nYAj+9F7uInSfC7qO7a4Utu+m5PjgxUoc=	\N	f			f	t	2022-03-15 06:50:10.736017+00		Kirill	1fa44633919b4fec8be6cac9f690165f	\N
1	pbkdf2_sha256$260000$28hNq99YgmXWhApLoboCZW$M5qLYOJEhCWqCbHzvABCzGZeKW6Ar9Jv/pqIBKZ2ROo=	2023-06-26 20:12:57.50609+00	t	Дмитрий		t	t	2022-03-14 19:32:48+00	chinzhin@mail.ru	admin	c6ed1f3e5de34842b8ffc94d596f69d9	user/avatar/IMG_20230323_003419_241_JAzCiEm.jpg
2	pbkdf2_sha256$260000$E9bZ2NhBU66SXAzALuvPCI$gRPDcQk8zSaarch7m1/XYY2eKRIc8DtSavwjCOpO0dM=	2022-03-19 13:57:40.999164+00	f			f	t	2022-03-15 06:36:57.591464+00		chinzhin	f8e569fa395a40e89aadeb06dc40401f	\N
10	pbkdf2_sha256$260000$N6Rp7TVhbpe8NzSRge2OY7$LN83mwLY7kEF1bdxrKNe2OoyGwWAwB466R5U9OR0acM=	2022-03-25 18:50:45.958956+00	t			t	t	2022-03-25 18:27:01.233056+00		Azad	06ef27505d52438796f842ede1ddf3e6	\N
8	pbkdf2_sha256$260000$UF8Wd4bsWzK11omreXqf2c$0SPvybNvav7dF279GFj2+aAg0jN0ao4fxe7xeiYEicw=	2022-03-15 12:58:53+00	f			f	t	2022-03-15 06:53:59+00		Dog_Bite	09eeda922dd547ff90a0744d637cdca8	user/avatar/IMG_20230323_003059_555.jpg
6	pbkdf2_sha256$260000$Hg675QBb5fIEaLNYcVSXDW$k8p8yeF7tfVlpvTnpXM8bw72hcHdVcbcR3ZMJ65IJds=	2022-03-15 12:50:11+00	f			f	t	2022-03-15 06:49:27+00		HeiSeven	ef09b3310857495eaa00e466a07d9934	user/avatar/IMG_20230323_003042_634.jpg
4	pbkdf2_sha256$260000$vnLZnIzChVvTNwBozyUtay$HnsNL//bXa8TGa20KPZ6fxDNbubiJ0GtXVjkqvxUF38=	2022-03-15 06:48:04+00	f			f	t	2022-03-15 06:47:51+00		Pavel	fb0488dc590c4b0db54065b05e9d35fa	user/avatar/IMG_20230323_003116_940.jpg
3	pbkdf2_sha256$260000$IYxH4OzQFwVkrPPOLs1tn2$VKi25E7qDZXtZrllz4OdnoWf8oNfAzfpgj7EaPSbQNU=	2022-03-15 06:47:28+00	f			f	t	2022-03-15 06:47:13+00		xakep291	212ff9ec82ea42d2b30ce58a559e6692	user/avatar/IMG_20230323_003122_949.jpg
\.


--
-- Data for Name: chatauth_user_friends; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chatauth_user_friends (id, from_user_id, to_user_id) FROM stdin;
\.


--
-- Data for Name: chatauth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chatauth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: chatauth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chatauth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: chitchat_chat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chitchat_chat (id, name, chat_type, logo, owner_id) FROM stdin;
37	admin|Dog_Bite	private		\N
1	Основной чат	public	chat/logo/IMG_20230323_003640_354.jpg	1
38	admin|Pavel	private		\N
39	admin|xakep291	private		\N
40	admin|HeiSeven	private		\N
42	admin|alena	private		\N
\.


--
-- Data for Name: chitchat_message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chitchat_message (id, "timestamp", timeupdate, text, author_id, chat_id, edited, meta, read, reply_to_id, sent) FROM stdin;
270	2023-03-22 20:44:50.337896+00	2023-03-22 20:44:50.337943+00	Привет	1	42	f	{}	f	\N	f
271	2023-04-02 16:24:28.947705+00	2023-04-02 16:24:28.947749+00	Куку	1	42	f	{}	f	\N	f
7	2022-03-15 06:57:47.667694+00	2022-03-15 06:57:47.667786+00	Всем привет	1	1	f	{}	f	\N	f
8	2022-03-15 06:57:47.87941+00	2022-03-15 06:57:47.879445+00	ЕЕЕЕ	6	1	f	{}	f	\N	f
9	2022-03-15 06:58:04.725354+00	2022-03-15 06:58:04.725541+00	Третий НАХ!	8	1	f	{}	f	\N	f
11	2022-03-15 06:58:19.636656+00	2022-03-15 06:58:19.636778+00	Ништяк	1	1	f	{}	f	\N	f
12	2022-03-15 06:59:28.476599+00	2022-03-15 06:59:28.476787+00	Работает однако	1	1	f	{}	f	\N	f
13	2022-03-15 07:00:15.018267+00	2022-03-15 07:00:15.018498+00	У меня время поправилось!!	8	1	f	{}	f	\N	f
14	2022-03-15 07:00:28.115589+00	2022-03-15 07:00:28.115712+00	новое	1	1	f	{}	f	\N	f
16	2022-03-15 07:02:49.769655+00	2022-03-15 07:02:49.769808+00	🎉	8	1	f	{}	f	\N	f
17	2022-03-15 08:01:41.156714+00	2022-03-15 08:01:41.156845+00	Go77	8	1	f	{}	f	\N	f
18	2022-03-15 08:12:58.44255+00	2022-03-15 08:12:58.442654+00	🚬	8	1	f	{}	f	\N	f
19	2022-03-15 08:39:16.482962+00	2022-03-15 08:39:16.483138+00	💆🏌️💇🤸🏋️🍝	1	1	f	{}	f	\N	f
20	2022-03-15 08:42:48.465311+00	2022-03-15 08:42:48.465436+00	Теперь доступно на мобилке)	1	1	f	{}	f	\N	f
22	2022-03-15 12:34:12.836068+00	2022-03-15 12:34:12.836134+00	sdf	1	1	f	{}	f	\N	f
23	2022-03-15 12:46:41.563802+00	2022-03-15 12:46:41.563917+00	Привет 	1	1	f	{}	f	\N	f
24	2022-03-15 12:51:09.380759+00	2022-03-15 12:51:09.380887+00	Ку\n	1	1	f	{}	f	\N	f
25	2022-03-15 12:51:11.286508+00	2022-03-15 12:51:11.286561+00	Воу 	6	1	f	{}	f	\N	f
26	2022-03-15 12:51:19.769882+00	2022-03-15 12:51:19.770033+00	Круто а\n	1	1	f	{}	f	\N	f
27	2022-03-15 12:51:41.996788+00	2022-03-15 12:51:41.996944+00	С мобилки нах!! ✌️	8	1	f	{}	f	\N	f
28	2022-03-15 12:52:23.098632+00	2022-03-15 12:52:23.098815+00	Время	1	1	f	{}	f	\N	f
29	2022-03-15 12:52:37.466641+00	2022-03-15 12:52:37.466702+00	Араво	8	1	f	{}	f	\N	f
32	2022-03-15 14:12:43.309583+00	2022-03-15 14:12:43.309625+00	😍😍😍😍	1	1	f	{}	f	\N	f
34	2022-03-15 14:15:06.997559+00	2022-03-15 14:15:06.997716+00	😍🥰🍝🏋️🤸💇🏌️💆🫴	1	1	f	{}	f	\N	f
36	2022-03-15 14:15:25.281901+00	2022-03-15 14:15:25.282048+00	😁	6	1	f	{}	f	\N	f
37	2022-03-15 14:15:31.591647+00	2022-03-15 14:15:31.591719+00	Ахаха\n	1	1	f	{}	f	\N	f
38	2022-03-15 14:56:46.542486+00	2022-03-15 14:56:46.542659+00	🦖	8	1	f	{}	f	\N	f
49	2022-03-16 05:32:27.252046+00	2022-03-16 05:32:27.252201+00	Всем добрейшего	1	1	f	{}	f	\N	f
51	2022-03-16 08:07:21.125311+00	2022-03-16 08:07:21.125455+00	👍🏾	8	1	f	{}	f	\N	f
52	2022-03-16 08:07:59.724061+00	2022-03-16 08:07:59.724201+00	🚬	1	1	f	{}	f	\N	f
53	2022-03-16 08:08:31.871491+00	2022-03-16 08:08:31.871645+00	🐝	8	1	f	{}	f	\N	f
54	2022-03-16 08:08:50.151789+00	2022-03-16 08:08:50.152129+00	🌬️	1	1	f	{}	f	\N	f
55	2022-03-17 10:52:35.723421+00	2022-03-17 10:52:35.723548+00	)))\n\n	1	1	f	{}	f	\N	f
66	2022-03-18 11:02:31.843137+00	2022-03-18 11:02:31.843255+00	Есть кто живой?	8	1	f	{}	f	\N	f
71	2022-03-19 05:16:27.677041+00	2022-03-19 05:16:27.677219+00	Я живой)) Надо как-то ПУШки сделать.\n	1	1	f	{}	f	\N	f
74	2022-03-19 13:58:05.709392+00	2022-03-19 13:58:05.709582+00	Теперь уже дизайн повеселее в сообщениях )\n	2	1	f	{}	f	\N	f
75	2022-03-19 13:58:48.653102+00	2022-03-19 13:58:48.65318+00	Есть \n	2	1	f	{}	f	\N	f
76	2022-03-19 14:02:41.221826+00	2022-03-19 14:02:41.222037+00	Дальше больше...\n	2	1	f	{}	f	\N	f
77	2022-03-19 14:02:49.678432+00	2022-03-19 14:02:49.678507+00	\n	2	1	f	{}	f	\N	f
78	2022-03-19 14:02:52.798539+00	2022-03-19 14:02:52.798594+00	\n	2	1	f	{}	f	\N	f
79	2022-03-19 14:02:55.371742+00	2022-03-19 14:02:55.371787+00	\n	2	1	f	{}	f	\N	f
10	2022-03-15 06:58:10.07659+00	2022-03-15 06:58:10.076687+00	привет	\N	1	f	{}	f	\N	f
33	2022-03-15 14:14:40.575997+00	2022-03-15 14:14:40.576168+00	❤❤	\N	1	f	{}	f	\N	f
35	2022-03-15 14:15:15.965074+00	2022-03-15 14:15:15.965122+00	🍕🍕\n	\N	1	f	{}	f	\N	f
50	2022-03-16 06:32:21.574427+00	2022-03-16 06:32:21.574593+00	Доброе)\n	\N	1	f	{}	f	\N	f
85	2022-03-19 19:33:49.695117+00	2022-03-19 19:33:49.695266+00	Ничего не происходит\nНичего не происходит\n\nНичего не происходит\nНичего не происходит\n\n Ничего не происходит\nНичего не происходит\n\nНичего не происходит\nНичего не происходит	2	1	f	{}	f	\N	f
86	2022-03-19 19:35:00.014936+00	2022-03-19 19:35:00.015112+00	Голову в коробке пластмассовой\nВместе с телефоном, ключами и паспортом\nРентгеном в сканере просветят ласково\nВ целях безопасности государственной\nИ обратно вряд ли пришьют уже:\nРуки ищут затылки над обрубками шей\nТуловища жмутся грудными клетками к стенам\nНе видно из контейнеров, что там с телом\nЛента с монотонным гулом коробку тащит\nИ для тулова тоже подыщут ящик:\nСтанет стан обезглавленный, в нём лежащий\nПочти покойником, почти настоящим\nКто-то, может быть, даже помянет на 40 дней\n\n[Припев: Егор Летов]\nВсё как у людей\nВсё как у людей\n\n[Куплет 2: Noize MC]\nА девочка из конной полиции\nС белым айфоном на белом жеребце\nЖдёт в Инстаграме лайка от принца\nИ ждёт от Чёрной Пятницы низких цен\nМерно цокают копытца по улице\nВинтят демонстрантов мальчики-коллеги\nОбсуждают рецепты подружки-умницы\nВ недозаблокированной «телеге»\nОна завтра купит в «Меге» наборчик «LEGO»\nДля племянника на его рождения день\nВ наборе — автозак и три человека:\nДвое ментов на одного — всё как у людей\nВсё как у людей\n\n\n[Припев: Егор Летов]\nВсё как у людей\nВсё как у людей\n\n[Куплет 3: Noize MC]\nСМИ голосят то, что повсюду агенты МИ-6, ЦРУ и Моссада\nПугают исчадием ада звёздно-полосато-носато-пейсатым\nЛёд под ногами майора ломается легче, чем корка фалафеля:\nМайор не боится, майор не утонет — майор, если что, в батискафе\nТы слышишь, как скрепы скрипят и жужжат старомодным дабстепом?\nЭто снова, словно Челубей с Пересветом, насмерть бьются «совдеп» с Госдепом\nА чтобы чужие боялись, своих посильнее бей\n\n[Припев: Егор Летов]\nВсё как у людей\nВсё как у людей\n\n[Куплет 4: Noize MC]\nСлышь, не нагнетай, перестань —\nЕсли кипеш реальный начнётся, что будет, представь!\nЕсли тут тебе неймётся — так пиздуй в Пиндостан:\nХули ты всё надрываешься? В натуре, достал!\nОт дерьма на вентиляторе дышать не легче —\nЛишь охота респираторы прижать покрепче\nБез тебя хватает смрада — орать легко\nВсе знают, как не надо, как быть — никто\nПолитшапито. Медиаклоунада:\nУ каждой хламидомонады — ума палата\nИ на всё — свои взгляды\nА ты что-то пиздишь многовато нам тут\nДля заложника режима, сука, с кляпом во рту\nПротест невзатяг, бунт в соцсетях\nПляски под чужую дудку на чужих костях\nЛучше бы шишек ГАЗПРОМовских развлекал\nПо жирным заказникам в шикарных особняках\nМир однокомнатных квартир с совмещённым санузлом:\nМикрофон за 100 рублей, самопальное музло\nТы же выбрался оттуда? Значит, повезло!\nТак фильтруй базар и множь на счету число:\nУютная коробка мещанского райка\nЛубочные картинки про счастье дурака\nТипичные сюжеты — культуры суррогат:\nДушонка — по дешёвке, а шкура — дорога\nНо ты давай вон спроси у своих детей\nГде их папа нужней: в сводках новостей\nЧтоб ноги об него вытирать по телику?\nИли в коридоре, чтоб сиденье поднять у велика?\nСкучно, что ли, живётся без пиздюлей?\nПока кормят — ешь, пока поят — пей\n[Припев: Егор Летов]\nВсё как у людей\nВсё как у людей\nВсё как у людей\nВсё как у людей\n\n[Аутро: Объявление]\nЭто ваши сыновья, будьте благоразумны, не нарушайте общ—\n\n\n\n28\n\nEmbed\nAbout\nGenius Annotation\n3 contributors\nВ честь дня рождения Егора Летова Noize MC представил трек, который вошёл в сборник под названием «Без меня. Трибьют Егора Летова».\n\nОгромное количество известных и не очень артистов перепели песни великого музыканта и поэта, однако Noize MC решил лишь засемплировать припев оригинальной песни и дописать к ней четыре куплета. Трек «Всё как у людей» выпущен как бонус для сборника.\n\nNoize MC также представил клип, где он на фоне различных картин абстрактно и сатирически описывает происходящее там, ассоциируя с политическими событиями в стране:\n\n\nВыпуск клипа сразу же навёл много ажиотажа. Он сразу разлетелся по людям в силу своей релевантности к событиям в стране.\n\nВозможно, дополнительный контраст добавляет то, что за несколько дней до этого известные рэперы Тимати и Гуф выпустили клип-пропаганду — чем вызвали большой всплеск негатива; вскоре клип попал в топ самых задизлайканных видео в рунете.\n\nExpand \nAsk us a question about this song\n \nAsk a question *\nWhat have the artists said about the song?\n\nGenius Answer\n1 contributor\nДля меня творчество Егора Летова — это самое трушное и оголтелое, что вообще было в русском роке.\nМоя группа по сути ведет свой отсчет с рэпкор-кавера на «Все идет по плану». Это был первый трек, который мы записали в общаге РГГУ вместе с нашим бессменным басистом Саней «Кислым» в 2003 году.\nЯ очень люблю песню «Все как у людей», могу слушать на повторе по полчаса. Знаю, что и для самого Егора это была особенная песня. Она пронзительная и бесконечно крутая.\nОт того, насколько песни Летова актуальны сегодня, иногда просто жутко становится.\n\nИсточник: Иван Алексеев для портала Meduza\n\n\nManage\nБез меня. Трибьют Егора Летова (Without me. Tribute to Egor Letov) (2019)\nVarious Artists\n1.\nМы — лёд (We — ice)\n2.\nСлава психонавтам (Glory to the psychonauts)\n3.\nВсё идёт по плану (Everything Goes According To Plan)\n4.\nПро дурачка (About the Fool)\n5.\nМама, мама... (Mother, mother...)\n6.\nИ снова темно (And it’s dark again)\n7.\nДолгая. Счастливая. Жизнь. (Long. Happy. Life.)\n8.\nНа хуй (Fuck it)\n9.\nНечего терять (Nothing to lose)\n12.\nСнаружи всех измерений (Outside of all Dimensions)\n17.\nПошли вы все на хуй (Fuck You All)\n18.\nВсё как у людей (Just Like People)\n23.\nПанк и рок-н-ролл (Punk & rock’n’roll)\n25.\nВселенская Большая Любовь (World’s Big Love)\n26.\nВинтовка - это праздник (Rifle is a holiday)\n27.\nВсё как у людей (Everything is Like People’s)\nCredits\nWritten By\nЕгор Летов (Egor Letov) & Noize MC\nMastering Engineer\nВадим Некрасов (Vadim Nekrasov)\nMixing Engineer\nВадим Некрасов (Vadim Nekrasov)\nLabel\nUniversal Music Group\nRelease Date\nSeptember 10, 2019\nSamples\nВсё как у людей (Just Like People) by Гражданская оборона (Civil Defense)\nInterpolates\nОтряд не заметил потери бойца (The squad did not notice the loss of a fighter) by Егор и Опизденевшие (Egor i Opizdenevshie) & Мы — лёд (We — ice) by Гражданская оборона (Civil Defense)\nRemixed By\nВсё как у людей (Everything is Like People’s) [Album Version] by Noize MC\nTags\nRap\nRock\nRap Rock\nРусский рок (Russian Rock)\nРусский рэп (Russian Rap)\nРоссия (Russia)\nExpand \n\nComments\nAdd a comment\nArabor\nEditor\n3 years ago\nТрек Noize MC уже на 8-м месте по всему миру!\n\n\n\n+27\nArabor\nEditor\n3 years ago\nТрек Noize MC попал в ТОП-10 по всему миру!\n\n\n\n+13\nlemonkesuperfan\n3 years ago\n1945\n\n+2\nmon_jenner\n3 years ago\nK es esto\n\n+1\nExpand \nSign Up And Drop Knowledge 🤓\nGenius is the ultimate source of music knowledge, created by scholars like you who share facts and insight about the songs and artists they love.\nSign Up\nGenius is the world’s biggest collection of song lyrics and musical knowledge\nAbout Genius\nContributor Guidelines\nPress\nAdvertise\nEvent Space\nPrivacy Policy\nLicensing\nJobs\nDevelopers\nCopyright Policy\nContact Us\nSign In\nDo Not Sell My Personal Information\nVERIFIED ARTISTS\nALL ARTISTS:\nA\nB\nC\nD\nE\nF\nG\nH\nI\nJ\nK\nL\nM\nN\nO\nP\nQ\nR\nS\nT	2	1	f	{}	f	\N	f
87	2022-03-19 19:35:30.156708+00	2022-03-19 19:35:30.156886+00	Аллаьаьа\nАлплпла\nАлплпла\nПлплпл\nПлплпл\nПлплпл\nПлплпл\nПока\nА\nА\nП\nП\nП	2	1	f	{}	f	\N	f
92	2022-03-19 19:38:24.146379+00	2022-03-19 19:38:24.146532+00	Тестирование в полном разгаре	2	1	f	{}	f	\N	f
95	2022-03-21 14:14:13.756047+00	2022-03-21 14:14:13.756211+00	Обажаю песню пошли вы все на хуй!	8	1	f	{}	f	\N	f
96	2022-03-21 14:14:38.821581+00	2022-03-21 14:14:38.821726+00	Тут так много нового всего!	8	1	f	{}	f	\N	f
97	2022-03-21 14:14:45.426916+00	2022-03-21 14:14:45.42702+00	Торчу..	8	1	f	{}	f	\N	f
98	2022-03-21 14:14:51.220419+00	2022-03-21 14:14:51.220537+00	е	8	1	f	{}	f	\N	f
99	2022-03-21 18:32:34.977658+00	2022-03-21 18:32:34.977823+00	Диман заценил уже	1	1	f	{}	f	\N	f
100	2022-03-21 19:41:46.229533+00	2022-03-21 19:41:46.229673+00	Дизайн чуток получше стал)	1	1	f	{}	f	\N	f
101	2022-03-22 10:10:12.76544+00	2022-03-22 10:10:12.765614+00	🤪	8	1	f	{}	f	\N	f
102	2022-03-22 15:52:03.271945+00	2022-03-22 15:52:03.272126+00	Ыыы 	6	1	f	{}	f	\N	f
116	2022-04-24 17:59:01.134263+00	2022-04-24 17:59:01.134583+00	Боковое меню теперь автоматически обновляется	1	1	f	{}	f	\N	f
117	2022-04-24 17:59:19.291605+00	2022-04-24 17:59:19.291677+00	Но это не точно	1	1	f	{}	f	\N	f
119	2022-04-24 20:21:17.238705+00	2022-04-24 20:21:17.238753+00	Точно	1	1	f	{}	f	\N	f
139	2022-04-29 14:08:44.613371+00	2022-04-29 14:08:44.613482+00	вапвапвап	1	1	f	{}	f	\N	f
140	2022-04-29 14:08:51.453847+00	2022-04-29 14:08:51.453901+00	вапвап	1	1	f	{}	f	\N	f
141	2022-04-29 14:08:56.609425+00	2022-04-29 14:08:56.609496+00	вапвп	1	1	f	{}	f	\N	f
143	2022-04-30 08:01:07.358931+00	2022-04-30 08:01:07.359057+00	rere	1	1	f	{}	f	\N	f
144	2022-04-30 08:01:23.287106+00	2022-04-30 08:01:23.287181+00	Вроде работает	1	1	f	{}	f	\N	f
145	2022-04-30 08:01:24.24433+00	2022-04-30 08:01:24.244419+00	фыв	1	1	f	{}	f	\N	f
146	2022-04-30 08:01:24.616126+00	2022-04-30 08:01:24.616175+00	фыв	1	1	f	{}	f	\N	f
147	2022-04-30 08:01:24.980934+00	2022-04-30 08:01:24.981035+00	фыв	1	1	f	{}	f	\N	f
148	2022-04-30 08:01:25.271232+00	2022-04-30 08:01:25.271271+00	фыв	1	1	f	{}	f	\N	f
149	2022-04-30 08:01:25.620711+00	2022-04-30 08:01:25.620747+00	фыв	1	1	f	{}	f	\N	f
150	2022-04-30 13:19:05.541688+00	2022-04-30 13:19:05.541856+00	Кукушка	1	1	f	{}	f	\N	f
\.


--
-- Data for Name: chitchat_userchat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.chitchat_userchat (id, chat_id, user_id) FROM stdin;
1	1	1
82	38	1
83	38	4
84	39	1
85	39	3
86	40	1
87	40	6
10	1	2
11	1	3
12	1	4
90	42	1
14	1	6
15	1	7
16	1	8
91	42	9
80	37	1
81	37	8
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2022-04-29 06:58:42.71053+00	17	Алло	2	[{"changed": {"fields": ["\\u041d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b", "Owner"]}}]	6	1
2	2022-04-29 06:58:57.258273+00	16	Чат суперчат	2	[{"changed": {"fields": ["\\u041d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b", "Owner"]}}]	6	1
3	2022-04-29 06:59:16.392692+00	12	Очень длинное название чата не помещается в экран	2	[{"changed": {"fields": ["\\u041d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b", "Owner"]}}]	6	1
4	2022-04-29 06:59:30.047066+00	11	Админ чат	2	[{"changed": {"fields": ["\\u041d\\u0430\\u0437\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b", "Owner"]}}]	6	1
5	2022-04-29 06:59:41.440137+00	9	Курильщики	2	[{"changed": {"fields": ["Owner"]}}]	6	1
6	2022-04-29 07:00:12.837432+00	9	Курильщики	2	[{"changed": {"fields": ["Owner"]}}]	6	1
7	2022-04-29 07:00:40.904776+00	31	UserChat object (31)	1	[{"added": {}}]	7	1
8	2022-04-29 07:00:56.841181+00	9	Курильщики	3		6	1
9	2022-04-29 07:01:06.580245+00	10	123	3		6	1
10	2022-04-29 07:01:06.58802+00	8	test	3		6	1
11	2022-04-29 07:01:06.596+00	6	1	3		6	1
12	2022-04-29 07:01:06.602559+00	3	Test2	3		6	1
13	2022-04-29 07:01:06.608519+00	2	Test	3		6	1
14	2022-04-30 17:19:52.600036+00	1	Основной чат	2	[{"changed": {"fields": ["Owner", "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	6	1
15	2022-04-30 17:20:18.724662+00	4	Хорошо	2	[{"changed": {"fields": ["Owner", "\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	6	1
16	2023-02-26 07:38:54.42281+00	1	admin	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	9	1
17	2023-03-22 20:27:13.765252+00	12	dima1	3		9	1
18	2023-03-22 20:27:13.775917+00	5	Кристина	3		9	1
19	2023-03-22 20:27:13.785153+00	11	Чертовщина	3		9	1
20	2023-03-22 20:27:35.176528+00	34	admin|alena	3		6	1
21	2023-03-22 20:27:35.181331+00	33	dima1|alena	3		6	1
22	2023-03-22 20:27:35.185969+00	31	admin|Кристина	3		6	1
23	2023-03-22 20:27:35.18932+00	30	admin|Чертовщина	3		6	1
24	2023-03-22 20:27:35.192955+00	29	admin|alena	3		6	1
25	2023-03-22 20:31:55.056669+00	8	Dog_Bite	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	9	1
26	2023-03-22 20:32:06.517835+00	6	HeiSeven	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	9	1
27	2023-03-22 20:32:20.483033+00	4	Pavel	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	9	1
28	2023-03-22 20:32:34.652848+00	3	xakep291	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	9	1
29	2023-03-22 20:35:37.520381+00	1	Основной чат	2	[{"changed": {"fields": ["\\u0422\\u0438\\u043f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u044b"]}}]	6	1
30	2023-03-22 20:36:55.428236+00	1	Основной чат	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	6	1
31	2023-03-22 20:38:41.73836+00	26	Пиздень	3		6	1
32	2023-03-22 20:38:41.743362+00	23	Chinzhin	3		6	1
33	2023-03-22 20:38:41.746347+00	22	БГ	3		6	1
34	2023-03-22 20:38:41.749149+00	21	Бурда	3		6	1
35	2023-03-22 20:38:41.751786+00	20	выаыа	3		6	1
36	2023-03-22 20:38:41.754426+00	19	еще один чат	3		6	1
37	2023-03-22 20:38:41.757433+00	18	Новый чат	3		6	1
38	2023-03-22 20:38:41.760139+00	17	Алло	3		6	1
39	2023-03-22 20:38:41.76347+00	16	Чат суперчат	3		6	1
40	2023-03-22 20:38:41.765899+00	15	Тест	3		6	1
41	2023-03-22 20:38:41.776739+00	14	Алёна Дима	3		6	1
42	2023-03-22 20:38:41.780999+00	13	Дима Алена	3		6	1
43	2023-03-22 20:38:41.786206+00	12	Очень длинное название чата не помещается в экран	3		6	1
44	2023-03-22 20:38:41.790387+00	11	Админ чат	3		6	1
45	2023-03-22 20:38:41.794334+00	7	Чат	3		6	1
46	2023-03-22 20:38:41.799114+00	5	Супер чат	3		6	1
47	2023-03-22 20:38:41.802263+00	4	Хорошо	3		6	1
48	2023-03-22 20:41:10.677973+00	269	269	3		8	1
49	2023-03-22 20:41:50.489418+00	268	268	3		8	1
50	2023-03-22 20:41:50.495796+00	267	267	3		8	1
51	2023-03-22 20:41:50.499269+00	266	266	3		8	1
52	2023-03-22 20:41:50.502846+00	263	263	3		8	1
53	2023-03-22 20:41:50.506476+00	262	262	3		8	1
54	2023-03-22 20:41:50.509462+00	261	261	3		8	1
55	2023-03-22 20:41:50.51344+00	260	260	3		8	1
56	2023-03-22 20:41:50.516342+00	259	259	3		8	1
57	2023-03-22 20:41:50.519672+00	258	258	3		8	1
58	2023-03-22 20:41:50.522867+00	257	257	3		8	1
59	2023-03-22 20:41:50.526716+00	256	256	3		8	1
60	2023-03-22 20:41:50.530487+00	255	255	3		8	1
61	2023-03-22 20:41:50.535+00	254	254	3		8	1
62	2023-03-22 20:41:50.538704+00	253	253	3		8	1
63	2023-03-22 20:42:25.468797+00	41	admin|Pavel	3		6	1
64	2023-03-22 20:44:27.237992+00	9	alena	2	[{"changed": {"fields": ["\\u0418\\u0437\\u043e\\u0431\\u0440\\u0430\\u0436\\u0435\\u043d\\u0438\\u0435"]}}]	9	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	chitchat	chat
7	chitchat	userchat
8	chitchat	message
9	chatauth	user
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2022-03-14 19:30:10.89497+00
2	contenttypes	0002_remove_content_type_name	2022-03-14 19:30:10.929986+00
3	auth	0001_initial	2022-03-14 19:30:11.096027+00
4	auth	0002_alter_permission_name_max_length	2022-03-14 19:30:11.117456+00
5	auth	0003_alter_user_email_max_length	2022-03-14 19:30:11.135483+00
6	auth	0004_alter_user_username_opts	2022-03-14 19:30:11.156227+00
7	auth	0005_alter_user_last_login_null	2022-03-14 19:30:11.196132+00
8	auth	0006_require_contenttypes_0002	2022-03-14 19:30:11.208331+00
9	auth	0007_alter_validators_add_error_messages	2022-03-14 19:30:11.238234+00
10	auth	0008_alter_user_username_max_length	2022-03-14 19:30:11.276332+00
11	auth	0009_alter_user_last_name_max_length	2022-03-14 19:30:11.314756+00
12	auth	0010_alter_group_name_max_length	2022-03-14 19:30:11.338708+00
13	auth	0011_update_proxy_permissions	2022-03-14 19:30:11.373545+00
14	auth	0012_alter_user_first_name_max_length	2022-03-14 19:30:11.391633+00
15	chatauth	0001_initial	2022-03-14 19:30:11.556784+00
16	admin	0001_initial	2022-03-14 19:30:11.64268+00
17	admin	0002_logentry_remove_auto_add	2022-03-14 19:30:11.666472+00
18	admin	0003_logentry_add_action_flag_choices	2022-03-14 19:30:11.699937+00
19	chatauth	0002_user_username	2022-03-14 19:30:11.774081+00
20	chatauth	0003_auto_20220310_1911	2022-03-14 19:30:11.825476+00
21	chatauth	0004_user_room	2022-03-14 19:30:11.864319+00
22	chatauth	0005_user_friends	2022-03-14 19:30:11.975994+00
23	chitchat	0001_initial	2022-03-14 19:30:12.187064+00
24	chitchat	0002_userchat_unique_user_chat	2022-03-14 19:30:12.227987+00
25	sessions	0001_initial	2022-03-14 19:30:12.301229+00
30	chatauth	0006_user_logo	2023-02-26 07:37:37.584086+00
31	chitchat	0003_auto_20220428_1820	2023-02-26 07:37:37.734248+00
32	chitchat	0004_alter_chat_chat_type	2023-02-26 07:37:37.756899+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
m4camyidcprqqgqdl1e1ubyf7q0zt6xn	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nUMta:ZzsGSp6pkL-3LZyvqwBhdwIzkm6SXPFg1ds-3zSSfcA	2022-03-30 06:12:30.136161+00
peioa6gttkowq8e7vpdh89ql08v8kjx2	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nUp3N:lXZDoVLEB8EKKLJAtSqu6rbtBQ_TOa4SJwhfa5Cp9J4	2022-03-31 12:16:29.240699+00
qg8yfvid64d09tdlxw96ymmlf86uhad3	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nUvri:-cNMF7CqtA95-RKVC865w-DHiR5iufTNCDQf7cazSeo	2022-03-31 19:32:54.302334+00
e9mte3qirmhf5abylbfjroeb4vjvm60r	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nUw9J:fCIYgViFX61cQQCWRCxbpzgO4AGbri8NKy1WCnx13TY	2022-03-31 19:51:05.172737+00
glcam4cyof8mvhugf229akt10jy5lpm0	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nVIWc:Q3_qhBjPQYCVboRPsWAKUkQZirSvcG_v_DPdQKkP-9g	2022-04-01 19:44:38.107456+00
coam5jjsg7a454vgnq77nanajt9z1ca0	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nTrFH:MQCRNNy6aMKQeIUcWqbVSZpn_oUCZMlEn4wseWjRJMo	2022-03-28 20:24:47.741157+00
dw0duvctnq4gy2g1v6rrpic7syu8ci6q	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nVX9n:nX77Gj3kxBr6cWxhSvMC6ksm1u_NVWBl5peGC7QwdAE	2022-04-02 11:22:03.799625+00
yturmo2bvifl4qgv0wmfb5ab5q59imhd	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nVpBJ:F0wDOKHIO2HZFxusCSGhz6vUO8uxWXxCPnXbVGz5A8w	2022-04-03 06:36:49.394642+00
27m1ok42qi5lx14md4oewh9ec7rt9k13	.eJxVjMEOwiAQRP-FsyGyiyAevfsNZNkFqRqalPZk_HfbpAc9TTLvzbxVpGWucel5ioOoi0J1-O0S8TO3DciD2n3UPLZ5GpLeFL3Trm-j5Nd1d_8OKvW6rgmtO6ExAY5Qkg8ZzixSimFiDGB8AJvXZIvWIzlvBRIW7ygjFDbq8wXLzjdt:1nU0xs:ke3vbvpFqln6SWiJ-_t_NbMWBZl0th5mfb6LmY81mq4	2022-03-29 06:47:28.974431+00
lcrf6vivzf17vc37ri4gd1tr24uvw04j	.eJxVjEEOwiAQRe_C2hAKFKhL956BDMyMVA0kpV0Z765NutDtf-_9l4iwrSVunZY4ozgLK06_W4L8oLoDvEO9NZlbXZc5yV2RB-3y2pCel8P9OyjQy7cetAkjG0SjHAUFilNgZAdDYMdTTl6DNykny9kQ2Imc8WgxK0vajyjeH_xtOMg:1nU0yS:izRlTfHj9exgl2LRR-BcLFyRETS2gW5KcTWWjnmehKA	2022-03-29 06:48:04.236181+00
77o608meu89i2wq3jrb60zeaepki16nv	.eJxVjMEOwiAQRP-FsyELVCAevfsNZBdYqRpISnsy_rs06UFvk3lv5i0CbmsJW89LmJO4CAXi9FsSxmeuO0kPrPcmY6vrMpPcFXnQLm8t5df1cP8OCvYy1sRKgQUm77UzdoLMhJ6IHUxIiTVCtoN4w0gj-BS10Xx2Tic3RPH5AhBkOFc:1nXoen:9mWLA540YLhQtCac-HNNB0o4TMjOT4Uq2bz3T2hwvYw	2022-04-08 18:27:29.089043+00
4e160unf35i6ich7iz0wvjqymxxpkep0	.eJxVjMEOwiAQRP-FsyELVCAevfsNZBdYqRpISnsy_rs06UFvk3lv5i0CbmsJW89LmJO4CAXi9FsSxmeuO0kPrPcmY6vrMpPcFXnQLm8t5df1cP8OCvYy1sRKgQUm77UzdoLMhJ6IHUxIiTVCtoN4w0gj-BS10Xx2Tic3RPH5AhBkOFc:1nXp1J:KNVbW6HYAF50JcVjE6mk0sjWXHpHEF_J7qPlQ4SIZBY	2022-04-08 18:50:45.971084+00
62qzxkllv7hl7yin046o5n0s5h5prhc9	.eJxVjDEOwjAMRe-SGUWEOE7DyM4ZKie2SQG1UtNOiLtDpQ6w_vfef5me1qX2a5O5H9icDZrD75apPGTcAN9pvE22TOMyD9luit1ps9eJ5XnZ3b-DSq1-awmAhFjAF_Ed0xEwkSppl11R8pFQEgEAc1J00aUQOaaTZB9UHZj3BwMOOH4:1nU0zx:dmAH0mQjQrLBC_m3TCM_lKjcQvZVw-Oi_HS8qypQ_qY	2022-03-29 06:49:37.563788+00
4h85ybrsflp8v2po1r2c4b812thfnvsb	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1neFpw:01XwJsn0GyflOJyfIyyJL0AM19eKPYJNYhKtB-nVnpI	2022-04-26 12:41:36.519063+00
ml3925aojhgt5t4klqpq4a518qjoqi1q	.eJxVjEsOwjAMBe-SNYpqN05jluw5Q-XEKSmgVOpnhbg7VOoCtm9m3sv0sq2l35Y896OasyFz-t2ipEeuO9C71Ntk01TXeYx2V-xBF3udND8vh_t3UGQp37pJKDw4AkTAqEMA9U1MQRlc68mxuiTsKLXs0QcgCV2HzIEAKKM37w_EWTZl:1nU11K:pVGp5xl3G0janLY-qckL3CCXGBZHBcMjlLsXDw0LFp8	2022-03-29 06:51:02.404754+00
zakzayfdrxbcxr49jmcqqjbq3qtjguup	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nfgju:NKtcfCS-d7IgdxypGGGmJmf1O5ISRmzGuHSd6SnOMRc	2022-04-30 11:37:18.774679+00
a7rrf0p80665i2ijn9lenzhmwxknl01d	.eJxVjEEOwiAQRe_C2pBCp4W6dN8zkJlhkKqBpLQr491Nky50-997_60C7lsOe5M1LFFdlVeX342Qn1IOEB9Y7lVzLdu6kD4UfdKm5xrldTvdv4OMLR-1Bw89OQtpYDdam0w3JYcepO8AxoFFkJl9oiFJdDIBoXgjSCOy6dTnC-lBOM4:1nU14d:li2vGT2cNERAu1BZz0HgpHvie0tJSj86SEjsLLPGhuc	2022-03-29 06:54:27.409063+00
pxgebkymjnvwyqjowavz3zljkjp9qvhh	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nU6M5:r8u5zv0zQhdnMlrExlVQ8YbS56qysmpt20r0CtMyzE4	2022-03-29 12:32:49.593329+00
oulqral6pd9l2vchq24i8xslt0maqmwq	.eJxVjDEOwjAMRe-SGUWEOE7DyM4ZKie2SQG1UtNOiLtDpQ6w_vfef5me1qX2a5O5H9icDZrD75apPGTcAN9pvE22TOMyD9luit1ps9eJ5XnZ3b-DSq1-awmAhFjAF_Ed0xEwkSppl11R8pFQEgEAc1J00aUQOaaTZB9UHZj3BwMOOH4:1nU6ct:6G6_GosPQU59vZTMq_HjBxi-lcY6sLROuV9l5NWsIIs	2022-03-29 12:50:11.237774+00
runrdrjcz4r1jktaybkj1idv7d0i0jti	.eJxVjEEOwiAQRe_C2pBCp4W6dN8zkJlhkKqBpLQr491Nky50-997_60C7lsOe5M1LFFdlVeX342Qn1IOEB9Y7lVzLdu6kD4UfdKm5xrldTvdv4OMLR-1Bw89OQtpYDdam0w3JYcepO8AxoFFkJl9oiFJdDIBoXgjSCOy6dTnC-lBOM4:1nU6dg:v8n-srD_Mb0aw73Wt9GsijiyvxhacdxsNH0CHD1xzU8	2022-03-29 12:51:00.513234+00
bbn5uy699s7t47ekolz1fqjhpfx17yvh	.eJxVjEEOwiAQRe_C2pBCp4W6dN8zkJlhkKqBpLQr491Nky50-997_60C7lsOe5M1LFFdlVeX342Qn1IOEB9Y7lVzLdu6kD4UfdKm5xrldTvdv4OMLR-1Bw89OQtpYDdam0w3JYcepO8AxoFFkJl9oiFJdDIBoXgjSCOy6dTnC-lBOM4:1nU6lJ:JENjEMWL62vryQiTD0KanVqUN_qvc4e3O_Nj2r6rTg8	2022-03-29 12:58:53.601576+00
mkkcaf6t9jgrv74g1ttkqi5j5uzhjd57	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nU6mh:QegfJuVMYtAlCsiJNIZ5Q3X5U05swX1Vhk458Ibn0To	2022-03-29 13:00:19.417568+00
ehbncpzwfcid5prhsuot5ucj0po8ok8t	.eJxVjMsOwiAQRf-FtSHAFDq4dO83kBmYStWUpI-V8d-1SRe6veec-1KJtrWmbZE5jUWdVVSn340pP2TaQbnTdGs6t2mdR9a7og-66Gsr8rwc7t9BpaV-a-AS0DgLwTNSEMuSOwEkYGKONLjsPYL0yH1vokBnGQU7n91gHJJ6fwDwbzgH:1nUAbB:rN6ofaIcCpx9wJokB2A1oCFCreZwgLXYPPfGj0jabPM	2022-03-29 17:04:41.043624+00
qaylyswsz335zxer6mz0sakqgdzvdx1a	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nUAk2:XBF6JeonkqnwuhZcAOUbyFnNwrMf4rtS_cBBw_I8bdA	2022-03-29 17:13:50.703533+00
ueosn2yjxsv6qj1qmu0qzh43ttt013fo	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nUBnl:d6A7ngBwBa_irxVjUTCeMxy7Q5ULC1PHKoqGHOQGzoc	2022-03-29 18:21:45.672469+00
faarmr8j6yiyg3mf86nvcpmv7jzhcfb3	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pWINO:JBFRwQdx5KjX6_lud5H7hzJoERn07adYdTNQ4nxfXvc	2023-03-12 14:51:46.77508+00
q6jnevpepfvkv0ufa7fbbhwyriz8158k	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pZFox:PKZdYX-O5l1ZTgKHtgRlC37sfU58NJ2355SXMQUcCdY	2023-03-20 18:44:27.308308+00
xx8t993wzglvfb6yh6x7yds5oqjsx85g	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1phbQ0:UlrDzSp9ogezZ-M8nzvV1z-lPz8fuOBb_UchECrgbUM	2023-04-12 19:25:12.600262+00
2f3hc0cy45j9uzkwl9shgoo3z9orwivt	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nkBMS:zMjdtiSJodpOXrMsqW6HBDyWT31n4LaqSvxnZbXtXx0	2022-05-12 21:07:40.624593+00
cwue75ndfv1yayswkajfboh8g3o8fljd	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pWWjl:Hk7cjxiAU2qJkuW2Ce7y-C496-QIqdKx3FznEhJ67tY	2023-03-13 06:11:49.435349+00
bo8snxqxbfrj22xq0f6rjq80lcuzc0zu	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pf512:5pwiFnn6LPaDoKFEppz65dXEpSCBjcirqyDusYN5enE	2023-04-05 20:25:00.937833+00
4evjsc3l3mbgjcpiwp4246y8bvnbbhcr	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1phbQ1:YUk9iZkpywMzFAAFg8ZSAC_0xe1AhjkP4tzqu2Lrkgs	2023-04-12 19:25:13.174189+00
pl4af50cnkt3e3sxe3p0kep77wpr0xgg	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nkKa6:a2qLdDbldCqOyT9-gLGYgLWgxatfTfXQ7eZ-F9HB5X8	2022-05-13 06:58:22.646227+00
0kjghj4uepsuyvyb7lpxfpb1bln9aslz	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1psmU7:OkEsG512oE4daqfQcfYmYgf9bdrdRsPP0BT6AtR7NXc	2023-05-13 15:27:39.372707+00
1vnowkduilxuf4rbrlnyh613bf2cmwxs	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nkVVw:92s9JRv4KJm0TF2VROaVbtH5liwHPffKu7ISbl3KcKM	2022-05-13 18:38:48.219956+00
aycypqt6p07aanm305u61qboug5u8hxm	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1psmU7:OkEsG512oE4daqfQcfYmYgf9bdrdRsPP0BT6AtR7NXc	2023-05-13 15:27:39.871259+00
kndibp13nq5ipqbjidxm1dydyyrdzmfw	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nki2B:6XoBNnIK_u8nMoilKNX7DlS05cVv4Henq4NUU_AH47I	2022-05-14 08:00:55.380373+00
ufwq7flq58zqfhoaktpobp9wubw1wpvy	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1qDsa1:qSeV8cMZDWPfRM1sulpUUHRCzxLhoAYyonwlBHk6lyY	2023-07-10 20:12:57.570533+00
0hcs03n16qjf8z190nqdi9fhi0113fz0	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nklqX:BuOXpHbH6edEQvE7tSQjNdydc3W9feVeY2uvT31Ch00	2022-05-14 12:05:09.048242+00
fo7vtbnyhssxm3b5un2ct2fqrdfy8kbf	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nkrzp:TC50P80f8ASvtM2UevEvPgyEd-qpVrurje2mG91aEZQ	2022-05-14 18:39:09.056322+00
6qlfl0adckyxz3xa1k0lpv0rskchvmww	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nksHN:s8QZns5PxhFxbKA1pAEgDdn-1SANKhDSZJ_eVlZTstg	2022-05-14 18:57:17.670992+00
5f9bhjhapmt4s82e5fzm62ss9jd3scll	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nl8EH:81w1m1RXIFOEMdBpcmupjHh6OzYceakf6XB3zFTayY4	2022-05-15 11:59:09.499831+00
d782jhnp4jvlymcy31s4i0kqsbl52q4y	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nrg1T:OFeM03-Pm6NyDI-P9h1-JHQcRq80fhFP3p4ciW96FgE	2022-06-02 13:16:59.844424+00
iy10bcx03t94t6zetr18yx47y5a51o1i	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1nxYoG:k29_-H2ecM0fjK0b_wbjNq3NZy7i-o0XKSMrhvF7tjk	2022-06-18 18:47:40.466547+00
96jyqx4w72py6jo3tqhbt2bvx5gk9y20	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1o5qQi:kxf1AX9txgVbpc-nz071KXWUtUKNEI6ZDJphmh7EuQw	2022-07-11 15:13:36.56002+00
jhv30fsloz96cpfmqo0d5vqfmgiujmpa	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oDkX7:69kAgji7oIvF4J5oIt4jKULW7ze4z0MUji9uLKpv520	2022-08-02 10:32:53.840132+00
eu6fv4y5t86ee4isp8ca15a5q7pndjtv	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oDkX8:ddYk_FjtV2ayQYXHchB-TWkxIkZ6RxxUEf7rmD0zMM8	2022-08-02 10:32:54.454577+00
y9o11u98xl4sqg05kptcqmqqg7bkrtiu	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oUMmx:xHJvKr0Wp6hNsr8TA1DuE_p6JrRiqiGj1B7YFzPUTdE	2022-09-17 06:37:55.148933+00
z12abtkxxnu6qglbg376gu2keaoge6vb	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oUPyR:H4Mzetp57pvPv5KJhHNiRGg0AYl6N0bTz-JpoAl4pso	2022-09-17 10:01:59.95229+00
dqin7d3vddhyaoopcywlgcv9rpk4wtwv	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oUPyr:70dvGoLgP_2fpz-EjOLbhVsFEP9GwegEr_PAdgydAWI	2022-09-17 10:02:25.313794+00
u1uye3sii9b0hhu6c640b1louxrm0be4	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oVBJ6:JLAOlSzkjqKC6bCXDaufp4YqFDyA_9OmUPJ5-y9YX6w	2022-09-19 12:34:28.087972+00
g70wje6li6a5uckp25oyta8jbfxsgve9	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oVnfO:IE4ZS4H5M43avUC0WucV9d3RSCdChglgxIxIOFSk5-U	2022-09-21 05:32:02.429766+00
w4e8cxh1rqrulci3m534vo7svuwqc9nj	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oZSe7:EG2KD2HCzI3g4vF01STbfbSomCMskYqvvaFmZtOFP-g	2022-10-01 07:53:51.778637+00
qr548m58fzrie6ez9ni4diiv7awe6nq2	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1oZTpC:VPwwo8mpfAQRlKNC6O0zqdxv68sMsiZB6_6w9bOGBpQ	2022-10-01 09:09:22.873517+00
5y89nvp4lcxbywt40086wwcvddk3i0hx	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1olZqs:MOter08AcQ5Bz_LDImyJjrO594eMGcVXR9iYqzDPtGI	2022-11-03 18:01:06.779054+00
q46n0eou3b28t6r2ssnj674b3ai5b9d5	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1otDjD:xzW7lDnqDkLCXyBoGo8fwPNykj1iv5KqeHQVFqlXKMY	2022-11-24 20:00:47.751136+00
tfdcvn448nqguo4ecuano0w7dba2zul3	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p5B2v:KeOLoMF9JMlQbdHR0LTMkuy_MUeKrrIIOCxCFqgMZRw	2022-12-27 19:34:33.386389+00
73lz97w9zybzayrqpesb5vss79gydau6	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p5B2v:KeOLoMF9JMlQbdHR0LTMkuy_MUeKrrIIOCxCFqgMZRw	2022-12-27 19:34:33.855829+00
9w9sfjxznndawfjgn5jsqbbmw6p8nfgg	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p5B4N:h9V-xktxKO6OQsIeq2e256-orqnGYvchL5o1FiiEJLU	2022-12-27 19:36:03.103893+00
yr4rehpizup05g0twz6v9vop80zxg6b7	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p5B4N:h9V-xktxKO6OQsIeq2e256-orqnGYvchL5o1FiiEJLU	2022-12-27 19:36:03.891227+00
qg67forau91i1sd2s6kor973e0lc1etl	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p5B5M:NjN3pxK3OprUjn737qle2KclhcXa818sEpcSVMy5g6k	2022-12-27 19:37:04.298161+00
0ix7k76q8wan5wcawjtrxr1jiqy12am6	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p5LVE:ro_eMT21cv6ynXcctoStKsyaEGiRzefXOfj1i40S6X8	2022-12-28 06:44:28.485175+00
x24fh0m4shenyjbtos3471hnbbo0d0dh	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1p62Hr:P2JUCCgRzzsYniq7kZ1fpxWJsW8HXwX0dvrNw-O8Ffk	2022-12-30 04:25:31.412997+00
03y46zobqjnw6ocgrgztb62wu9fvd3mx	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pNeYk:sNSr_FumjzE4SSuFErmWaj1uPcolyDXXcp254eWUkmI	2023-02-16 18:43:46.223381+00
nj1ah0zrundqrnwvo8b1zhc7wcs15hrf	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pQBBG:INo5y6Ix4pKpeXlMzEaGVIEkD9LHyy2pf4iwFN4--AI	2023-02-23 17:57:58.934128+00
mlxkh8jnvf2m0cfd0i9md6ok40c9a48f	.eJxVjMsOwiAUBf-FtSG8Hy7d9xvI5QJSNZCUdmX8d9ukC93OzDlvEmBba9hGXsKcyJVwQS6_MAI-cztMekC7d4q9rcsc6ZHQ0w469ZRft7P9O6gw6r5WiqsCkmG0YJhOQksLGZV3sVjnk4hSaiweFM-lOARjnXaMg9mpwEI-XwgPOGw:1pQBIc:kaT98YpN63fjrq5xNAloW0DL70g0JlqI2DQwlKRci8E	2023-02-23 18:05:34.447806+00
kc9e4gsaxdx612w7c292emlbohi75aqk	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pQOGb:5iQz7O3yptEaGCNEnAUjRbFZbXqMW4HciX2r398u_Mo	2023-02-24 07:56:21.021784+00
da45y11op2e091mcpyguhsvrkvimd46k	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pQo7v:jns8QzjdvwFGB5gFmsAPIzRxMgakvnDbbS8LpEW2h2g	2023-02-25 11:33:07.220956+00
qx6c72kqs58klim7vcmfjuurk686e8a2	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pRh4d:u2tsNfmjgLg6UwylIViRxLXWLtv6qQUEU68Cyo9CVLk	2023-02-27 22:13:23.680585+00
6lwoywymjuy83uqt8599vdo40oxybin1	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pWBbv:nnAWHEgYrNZ5g_VYByaq4j-sHix2rfMvRD2hVjdppcM	2023-03-12 07:38:19.763026+00
5ktgkz7990kmh2wi8hxc7xf2vme2l14h	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pWCn9:7lhNdYfHaVQ3MSIkfv6SoUH2EXCsCxyCPr6bnB-TwFU	2023-03-12 08:53:59.145357+00
znnvzgx1198h0cf7e5zzaojqkhgw1yj5	.eJxVjMsOwiAQRf-FtSE8OpS6dO83kBkGpGogKe3K-O_apAvd3nPOfYmA21rC1tMSZhZnocXpdyOMj1R3wHestyZjq-syk9wVedAur43T83K4fwcFe_nWebARp-wyOR5HmxQkVOw9eLaT8kBGZSAHxpNyxmuEqBMOloAAdUbx_gDp1jf7:1pWCo7:9hMCoOeEBsWxInU5fmkJIdUKRzZRANoVlmEU912JxAM	2023-03-12 08:54:59.613337+00
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 36, true);


--
-- Name: chatauth_user_friends_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chatauth_user_friends_id_seq', 1, false);


--
-- Name: chatauth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chatauth_user_groups_id_seq', 1, false);


--
-- Name: chatauth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chatauth_user_id_seq', 12, true);


--
-- Name: chatauth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chatauth_user_user_permissions_id_seq', 1, false);


--
-- Name: chitchat_chat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chitchat_chat_id_seq', 42, true);


--
-- Name: chitchat_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chitchat_message_id_seq', 271, true);


--
-- Name: chitchat_userchat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.chitchat_userchat_id_seq', 91, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 64, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 9, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 32, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: chatauth_user_friends chatauth_user_friends_from_user_id_to_user_id_ffe35378_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_friends
    ADD CONSTRAINT chatauth_user_friends_from_user_id_to_user_id_ffe35378_uniq UNIQUE (from_user_id, to_user_id);


--
-- Name: chatauth_user_friends chatauth_user_friends_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_friends
    ADD CONSTRAINT chatauth_user_friends_pkey PRIMARY KEY (id);


--
-- Name: chatauth_user_groups chatauth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_groups
    ADD CONSTRAINT chatauth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: chatauth_user_groups chatauth_user_groups_user_id_group_id_1a377437_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_groups
    ADD CONSTRAINT chatauth_user_groups_user_id_group_id_1a377437_uniq UNIQUE (user_id, group_id);


--
-- Name: chatauth_user chatauth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user
    ADD CONSTRAINT chatauth_user_pkey PRIMARY KEY (id);


--
-- Name: chatauth_user_user_permissions chatauth_user_user_permi_user_id_permission_id_e15bffb0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_user_permissions
    ADD CONSTRAINT chatauth_user_user_permi_user_id_permission_id_e15bffb0_uniq UNIQUE (user_id, permission_id);


--
-- Name: chatauth_user_user_permissions chatauth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_user_permissions
    ADD CONSTRAINT chatauth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: chatauth_user chatauth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user
    ADD CONSTRAINT chatauth_user_username_key UNIQUE (username);


--
-- Name: chitchat_chat chitchat_chat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_chat
    ADD CONSTRAINT chitchat_chat_pkey PRIMARY KEY (id);


--
-- Name: chitchat_message chitchat_message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_message
    ADD CONSTRAINT chitchat_message_pkey PRIMARY KEY (id);


--
-- Name: chitchat_userchat chitchat_userchat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_userchat
    ADD CONSTRAINT chitchat_userchat_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: chitchat_userchat unique_user_chat; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_userchat
    ADD CONSTRAINT unique_user_chat UNIQUE (user_id, chat_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: chatauth_user_friends_from_user_id_dbacd5de; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_friends_from_user_id_dbacd5de ON public.chatauth_user_friends USING btree (from_user_id);


--
-- Name: chatauth_user_friends_to_user_id_14d99125; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_friends_to_user_id_14d99125 ON public.chatauth_user_friends USING btree (to_user_id);


--
-- Name: chatauth_user_groups_group_id_f90bb8ba; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_groups_group_id_f90bb8ba ON public.chatauth_user_groups USING btree (group_id);


--
-- Name: chatauth_user_groups_user_id_76fbe07b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_groups_user_id_76fbe07b ON public.chatauth_user_groups USING btree (user_id);


--
-- Name: chatauth_user_user_permissions_permission_id_7b8299d4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_user_permissions_permission_id_7b8299d4 ON public.chatauth_user_user_permissions USING btree (permission_id);


--
-- Name: chatauth_user_user_permissions_user_id_33da0b8a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_user_permissions_user_id_33da0b8a ON public.chatauth_user_user_permissions USING btree (user_id);


--
-- Name: chatauth_user_username_d1231925_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chatauth_user_username_d1231925_like ON public.chatauth_user USING btree (username varchar_pattern_ops);


--
-- Name: chitchat_chat_owner_id_c3718a56; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chitchat_chat_owner_id_c3718a56 ON public.chitchat_chat USING btree (owner_id);


--
-- Name: chitchat_message_author_id_376cdbc9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chitchat_message_author_id_376cdbc9 ON public.chitchat_message USING btree (author_id);


--
-- Name: chitchat_message_chat_id_b6a2bc3b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chitchat_message_chat_id_b6a2bc3b ON public.chitchat_message USING btree (chat_id);


--
-- Name: chitchat_message_reply_to_id_b6e3b119; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chitchat_message_reply_to_id_b6e3b119 ON public.chitchat_message USING btree (reply_to_id);


--
-- Name: chitchat_userchat_chat_id_69b26691; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chitchat_userchat_chat_id_69b26691 ON public.chitchat_userchat USING btree (chat_id);


--
-- Name: chitchat_userchat_user_id_8cf61344; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX chitchat_userchat_user_id_8cf61344 ON public.chitchat_userchat USING btree (user_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chatauth_user_friends chatauth_user_friends_from_user_id_dbacd5de_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_friends
    ADD CONSTRAINT chatauth_user_friends_from_user_id_dbacd5de_fk_chatauth_user_id FOREIGN KEY (from_user_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chatauth_user_friends chatauth_user_friends_to_user_id_14d99125_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_friends
    ADD CONSTRAINT chatauth_user_friends_to_user_id_14d99125_fk_chatauth_user_id FOREIGN KEY (to_user_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chatauth_user_groups chatauth_user_groups_group_id_f90bb8ba_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_groups
    ADD CONSTRAINT chatauth_user_groups_group_id_f90bb8ba_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chatauth_user_groups chatauth_user_groups_user_id_76fbe07b_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_groups
    ADD CONSTRAINT chatauth_user_groups_user_id_76fbe07b_fk_chatauth_user_id FOREIGN KEY (user_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chatauth_user_user_permissions chatauth_user_user_p_permission_id_7b8299d4_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_user_permissions
    ADD CONSTRAINT chatauth_user_user_p_permission_id_7b8299d4_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chatauth_user_user_permissions chatauth_user_user_p_user_id_33da0b8a_fk_chatauth_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chatauth_user_user_permissions
    ADD CONSTRAINT chatauth_user_user_p_user_id_33da0b8a_fk_chatauth_ FOREIGN KEY (user_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chitchat_chat chitchat_chat_owner_id_c3718a56_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_chat
    ADD CONSTRAINT chitchat_chat_owner_id_c3718a56_fk_chatauth_user_id FOREIGN KEY (owner_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chitchat_message chitchat_message_author_id_376cdbc9_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_message
    ADD CONSTRAINT chitchat_message_author_id_376cdbc9_fk_chatauth_user_id FOREIGN KEY (author_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chitchat_message chitchat_message_chat_id_b6a2bc3b_fk_chitchat_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_message
    ADD CONSTRAINT chitchat_message_chat_id_b6a2bc3b_fk_chitchat_chat_id FOREIGN KEY (chat_id) REFERENCES public.chitchat_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chitchat_message chitchat_message_reply_to_id_b6e3b119_fk_chitchat_message_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_message
    ADD CONSTRAINT chitchat_message_reply_to_id_b6e3b119_fk_chitchat_message_id FOREIGN KEY (reply_to_id) REFERENCES public.chitchat_message(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chitchat_userchat chitchat_userchat_chat_id_69b26691_fk_chitchat_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_userchat
    ADD CONSTRAINT chitchat_userchat_chat_id_69b26691_fk_chitchat_chat_id FOREIGN KEY (chat_id) REFERENCES public.chitchat_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: chitchat_userchat chitchat_userchat_user_id_8cf61344_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chitchat_userchat
    ADD CONSTRAINT chitchat_userchat_user_id_8cf61344_fk_chatauth_user_id FOREIGN KEY (user_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_chatauth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_chatauth_user_id FOREIGN KEY (user_id) REFERENCES public.chatauth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

