# chitchat
 Для установки требуются установленные программы:
 git + node js + python 3.8.8 (но может подойдет и 3.7.4)

## команда для клонирования репозитория:
```
git clone https://gitlab.com/dima386/chitchat.git
```

## install project
```
install_project.bat
```

## сборка frontend
```
npm run build
```


## запуск backend
```
run_backend.bat
```



## обновление картинок

```
vue-asset-generate -a icon.png -o img
```