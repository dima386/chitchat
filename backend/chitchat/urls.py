from . import views
from rest_framework import routers
from django.urls import path
router = routers.DefaultRouter()


router.register(r'room', views.ChatViewSet, basename='room')
router.register(r'message', views.MessageViewSet, basename='message')
router.register(r'groupchat', views.GroupChatViewSet, basename='groupchat')
router.register(r'directchat', views.DirectChatViewSet, basename='directchat')


urlpatterns = router.urls
urlpatterns += []
