from django.urls import path

from . import consumers

ws_urlpatterns = [
    path('stream/', consumers.ChatConsumer.as_asgi()),
]
