from rest_framework import serializers
# from .models import Message, Chat, UserChat
from . import models
from django.contrib.auth import get_user_model
from chatauth.serializers import UserListSerializer
User = get_user_model()


class MessageCreateSerializer(serializers.ModelSerializer):    
    class Meta:
        model = models.Message
        fields = '__all__'


class MessageSerializer(MessageCreateSerializer):
    author = UserListSerializer()   


class GroupChatListSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.GroupChat
        fields = ('logo', 'name')

        
class GroupChatRetrieveSerializer(serializers.ModelSerializer):
    users = UserListSerializer(many=True, read_only=True)

    class Meta:
        model = models.GroupChat
        fields = ('logo', 'name', 'users')


class DirectChatListSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    logo = serializers.SerializerMethodField()
    
    def get_peer_user(self, obj: models.DirectChat):
        request = self.context['request']
        if obj.from_user == request.user:
            return obj.to_user
        return obj.from_user

    def get_name(self, obj: models.DirectChat):
        return self.get_peer_user(obj).username
    
    def get_logo(self, obj):
        user = self.get_peer_user(obj)
        return user.logo.url if user and user.logo else None

    class Meta:
        model = models.DirectChat
        fields = ('logo', 'name')

class DirectChatRetrieveSerializer(DirectChatListSerializer):
    pass


class ChannelChatListSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ChannelChat
        fields = ('logo', 'name')
        

class ChannelChatRetrieveSerializer(serializers.ModelSerializer):
    users = UserListSerializer(many=True, read_only=True)

    class Meta:
        model = models.ChannelChat
        fields = ('logo', 'name', 'users')


class ChatListSerializer(serializers.ModelSerializer):
    last_msg = MessageSerializer(many=False, required=False)

    def to_representation(self, instance: models.Chat):
        match instance.chat_type:
            case models.ChatType.private.name:
                one2one_instance = instance.direct
                s_class = DirectChatListSerializer

            case models.ChatType.public.name:
                one2one_instance = instance.group
                s_class = GroupChatListSerializer

            case models.ChatType.channel.name:
                one2one_instance = instance.channel
                s_class = ChannelChatListSerializer
            case _:
                raise KeyError("[e]")
            
        instance.last_msg = instance.messages.all().last()
        ret = super().to_representation(instance)
        related_ret = s_class(instance=one2one_instance, context=self.context).data
        ret.update(related_ret)
        return ret

    class Meta:
        model = models.Chat
        read_only_fields = ('id', 'last_msg', 'chat_type')
        fields = read_only_fields


class ChatDetailSerializer(ChatListSerializer):
    
    class Meta:
        model = models.Chat
        read_only_fields = ('id', 'last_msg', 'chat_type', )
        fields = ('id', 'last_msg', 'chat_type')
    
    def to_representation(self, instance: models.Chat):
        match instance.chat_type:
            case models.ChatType.private.name:
                one2one_instance = instance.direct
                s_class = DirectChatRetrieveSerializer

            case models.ChatType.public.name:
                one2one_instance = instance.group
                s_class = GroupChatRetrieveSerializer

            case models.ChatType.channel.name:
                one2one_instance = instance.channel
                s_class = ChannelChatRetrieveSerializer
            case _:
                raise KeyError("[e]")
            
        instance.last_msg = instance.messages.all().last()
        ret = super().to_representation(instance)
        related_ret = s_class(instance=one2one_instance, context=self.context).data
        ret.update(related_ret)
        return ret


class GroupChatSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(many=True, queryset=User.objects.all(), required=False)
    
    class Meta:
        model = models.GroupChat
        fields = ('name', 'logo', 'users')
    

class DirectSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.DirectChat
        exclude = ('chat',)