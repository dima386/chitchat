from django.contrib import admin
from . import models
# Register your models here.



class ChatAdmin(admin.ModelAdmin):
    list_display = ("__str__", 'id')
    


admin.site.register(models.Chat, ChatAdmin)
admin.site.register(models.Message)

admin.site.register(models.GroupChat)
admin.site.register(models.ChannelChat)
admin.site.register(models.DirectChat)