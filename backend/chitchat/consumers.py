import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer, AsyncJsonWebsocketConsumer
from chatauth.serializers import UserListSerializer 
from django.utils import timezone, dateformat
from django.conf import settings
from chitchat.models import Message
from .exceptions import ClientError
from .utils import get_room_or_error, get_group_list, save_message, create_room, get_rooms_to_send, delete_message


class ChatConsumer(AsyncJsonWebsocketConsumer):
    commands = [
        'send_chat_delete_message', 
        'send_chat', 
        'join_room',
        'leave_room', 
        'add_room',
    ]

    async def connect(self):
        user = self.scope["user"]
        if user and user.is_authenticated:
            await self.accept()
            await self.join_room(user.room)
        else:
            await self.close()

    async def receive_json(self, data):
        command = data.get('command')
        try:
            if command in self.commands:
                callback = getattr(self, command)
                await callback(data)
        
        except ClientError as e:
            await self.send_json({"error": e.code})

    async def disconnect(self, code):
        user = self.scope["user"]
        try:
            await self.leave_room(user.room)
        except ClientError:
            print('какая-то ошибка')
            pass

    # Command helper methods called by receive_json
    async def join_room(self, room):
        await self.channel_layer.group_add(room, self.channel_name)
        
    async def leave_room(self, room):
        await self.channel_layer.group_discard(room, self.channel_name)

    async def send_chat_delete_message(self, data):
        chat = data["chat"]
        id = data["id"]
        await delete_message(id)
        rooms = await get_rooms_to_send(chat)
        for room in rooms:

            await self.channel_layer.group_send(
                room,
                {
                    "type": "msg.del",
                    "id": id,
                    "chat": chat,
                }
            )

    async def send_chat(self, data):
        chat = data["chat"]
        text = data["message"]
        user = self.scope["user"]
        serializer = UserListSerializer(user)
        message_db: Message = await save_message(user.id, chat, text)
        rooms = await get_rooms_to_send(chat)
        for room in rooms:

            await self.channel_layer.group_send(
                room,
                {
                    "type": "msg.new",
                    "id": message_db.pk,
                    "chat": chat,
                    "author": dict(serializer.data),
                    "text": text,
                    "timestamp": str(message_db.timestamp),
                }
            )

    ##### Handlers for messages sent over the channel layer

    # These helper methods are named by the types we send - so chat.join becomes chat_join
    async def chat_join(self, event):
        await self.send_json(
            {
                "msg_type": 'chat_join',
                "room": event["chat"],
                "author": event["author"],
            },
        )

    async def chat_leave(self, event):
        await self.send_json(
            {
                "msg_type": 'chat_leave',
                "chat": event["chat"],
                "author": event["author"],
            },
        )

    async def chat_new(self, event):
        await self.send_json(
            {
                "msg_type": 'chat_leave',
                "chat": event["chat"],
                "author": event["author"],
            },
        )

    async def msg_new(self, event):
        await self.send_json(
            {   
                "msg_type": "msg_new",
                "id": event["id"],
                "chat": event["chat"],
                "author": event["author"],
                "text": event["text"],
                "timestamp": event["timestamp"],
            },
        )

    async def msg_del(self, event):
        await self.send_json(
            {   
                "msg_type": "msg_del",
                "id": event["id"],
                "chat": event["chat"],
            },
        )
