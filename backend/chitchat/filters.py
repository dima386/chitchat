from rest_framework.filters import BaseFilterBackend


class IdOffsetFilter(BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        min_id = request.query_params.get('min_id')
        max_id = request.query_params.get('max_id')
        kw = {}
        if min_id: kw["id__gt"] = min_id
        if max_id: kw["id__lt"] = max_id
        return queryset.filter(**kw)
    

class ListIdFilter(BaseFilterBackend):
    filter_fields = 'list_id_fields'

    def filter_queryset(self, request, queryset, view):
        fields = getattr(view, self.filter_fields)
        kw = {}
        for f in fields:
            param = request.query_params.get(f)
            if param:
                kw[f] = param
        if kw:
            return queryset.filter(**kw)
        return queryset
