# Generated by Django 4.2.2 on 2023-11-06 19:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('chitchat', '0007_directchat_unique_directchat'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chat',
            name='logo',
        ),
        migrations.RemoveField(
            model_name='chat',
            name='name',
        ),
        migrations.RemoveField(
            model_name='chat',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='chat',
            name='users',
        ),
        migrations.DeleteModel(
            name='UserChat',
        ),
    ]
