# Generated by Django 4.2.2 on 2023-11-06 08:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('chitchat', '0004_alter_chat_chat_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChannelChat',
            fields=[
                ('chat', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='channel', serialize=False, to='chitchat.chat')),
                ('name', models.CharField(max_length=128, verbose_name='название комнаты')),
                ('logo', models.ImageField(blank=True, null=True, upload_to='chat/logo', verbose_name='изображение')),
                ('owner', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Канал',
                'verbose_name_plural': 'Канал',
            },
        ),
        migrations.CreateModel(
            name='GroupChat',
            fields=[
                ('chat', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='group', serialize=False, to='chitchat.chat')),
                ('name', models.CharField(max_length=128, verbose_name='название комнаты')),
                ('logo', models.ImageField(blank=True, null=True, upload_to='chat/logo', verbose_name='изображение')),
                ('owner', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Групповой чат',
                'verbose_name_plural': 'Групповой чат',
            },
        ),
        migrations.CreateModel(
            name='UserGroupChat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chitchat.groupchat')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Комнаты пользователей',
            },
        ),
        migrations.CreateModel(
            name='UserChannelChat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chat', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chitchat.channelchat')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Комнаты пользователей',
            },
        ),
        migrations.AddField(
            model_name='groupchat',
            name='users',
            field=models.ManyToManyField(blank=True, related_name='group_chats', through='chitchat.UserGroupChat', to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='DirectChat',
            fields=[
                ('chat', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='direct', serialize=False, to='chitchat.chat')),
                ('from_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='from_user', to=settings.AUTH_USER_MODEL)),
                ('to_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='to_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Личный чат',
                'verbose_name_plural': 'Личные чаты',
            },
        ),
        migrations.AddField(
            model_name='channelchat',
            name='users',
            field=models.ManyToManyField(blank=True, related_name='channel_chats', through='chitchat.UserChannelChat', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddConstraint(
            model_name='usergroupchat',
            constraint=models.UniqueConstraint(fields=('user', 'chat'), name='unique_GroupChat'),
        ),
        migrations.AddConstraint(
            model_name='userchannelchat',
            constraint=models.UniqueConstraint(fields=('user', 'chat'), name='unique_UserChannelChat'),
        ),
    ]
