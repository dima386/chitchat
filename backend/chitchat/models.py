from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()
import uuid
from enum import Enum as BaseEnum


class Enum(BaseEnum):
    @classmethod
    def choises(cls):
        return [(i.name, i.value) for i in cls]


class ChatType(Enum):
    private = 'private'
    public = 'public'
    channel = 'channel'


def uuid4_hex():
    return uuid.uuid4().hex


class Chat(models.Model):
    
    chat_type = models.CharField('тип комнаты', max_length=64, choices=ChatType.choises(), default=ChatType.public.value)

    class Meta:
        verbose_name = "Чат"
        verbose_name_plural = "Чаты"

    def __str__(self):
        return str(self.chat_type)


class DirectChat(models.Model):
    chat = models.OneToOneField(Chat, on_delete=models.CASCADE, related_name='direct', primary_key=True)
    from_user = models.ForeignKey(User, related_name='from_user', null=True, blank=True, on_delete=models.SET_NULL)
    to_user = models.ForeignKey(User, related_name='to_user', null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Личный чат"
        verbose_name_plural = "Личные чаты"
        constraints = [
            models.UniqueConstraint(
                fields=['from_user', 'to_user'],
                name='unique_DirectChat'
            )]

class GroupChat(models.Model):
    chat = models.OneToOneField(Chat, on_delete=models.CASCADE, related_name='group', primary_key=True)
    name = models.CharField("название комнаты", max_length=128)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    logo = models.ImageField('изображение', upload_to='chat/logo', blank=True, null=True)
    users = models.ManyToManyField(User, through='UserGroupChat', blank=True, related_name='group_chats')

    class Meta:
        verbose_name = "Групповой чат"
        verbose_name_plural = "Групповой чат"


class ChannelChat(models.Model):
    chat = models.OneToOneField(Chat, on_delete=models.CASCADE, related_name='channel', primary_key=True)
    name = models.CharField("название комнаты", max_length=128)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    logo = models.ImageField('изображение', upload_to='chat/logo', blank=True, null=True)
    users = models.ManyToManyField(User, through='UserChannelChat', blank=True, related_name='channel_chats')

    class Meta:
        verbose_name = "Канал"
        verbose_name_plural = "Канал"


class UserChannelChat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chat = models.ForeignKey(ChannelChat, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = 'Комнаты пользователей'
        verbose_name = 'Комнаты пользователей'
        constraints = [
            models.UniqueConstraint(
                fields=['user', 'chat'],
                name='unique_UserChannelChat'
            )]


class UserGroupChat(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chat = models.ForeignKey(GroupChat, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = 'Комнаты пользователей'
        verbose_name = 'Комнаты пользователей'
        constraints = [
            models.UniqueConstraint(
                fields=['user', 'chat'],
                name='unique_GroupChat'
            )]


class Message(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='messages')
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='messages')
    timestamp = models.DateTimeField('дата отправки', auto_now_add=True)
    timeupdate = models.DateTimeField('дата изменения', auto_now=True)
    text = models.TextField('текст сообщение')
    sent = models.BooleanField('отправлено', default=False)
    read = models.BooleanField('прочитано', default=False)
    edited = models.BooleanField('отредактировано', default=False)
    meta = models.JSONField('метаданные', default=dict)
    reply_to = models.ForeignKey('self', on_delete=models.SET_NULL, default=None, null=True, blank=True)
    
    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения'
