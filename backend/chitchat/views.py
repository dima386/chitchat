from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from . import models, serializers, filters
from django.db import IntegrityError

User = get_user_model()


class ChatViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ChatListSerializer

    def get_queryset(self):
        return models.Chat.objects.filter(
            Q(group__users=self.request.user) |
            Q(channel__users=self.request.user) |
            Q(direct__from_user=self.request.user) |
            Q(direct__to_user=self.request.user)
        ).distinct()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return serializers.ChatDetailSerializer
        return super().get_serializer_class()
    

class MessageViewSet(viewsets.ModelViewSet):
    queryset = models.Message.objects.all().order_by('-id')
    serializer_class = serializers.MessageSerializer
    filter_backends = [filters.IdOffsetFilter, filters.ListIdFilter]
    list_id_fields = ['chat']

    def get_serializer_class(self):
        if self.action in ['create', 'update']:
            return serializers.MessageCreateSerializer
        return super().get_serializer_class()
    
    def filter_queryset(self, queryset):
        limit = int(self.request.query_params.get('limit', 100))
        if limit:
            return super().filter_queryset(queryset)[:limit]
        return super().filter_queryset(queryset)


class GroupChatViewSet(viewsets.ModelViewSet):
    queryset = models.GroupChat.objects.all()
    serializer_class = serializers.GroupChatSerializer

    def perform_create(self, serializer: serializers.GroupChatSerializer):
        chat = models.Chat.objects.create(chat_type=models.ChatType.public.name)
        room: models.GroupChat = serializer.save(owner=self.request.user, chat=chat)
        room.users.add(self.request.user)


class DirectChatViewSet(viewsets.ModelViewSet):
    queryset = models.DirectChat.objects.all()
    serializer_class = serializers.DirectSerializer
    
    def perform_create(self, serializer: serializers.DirectSerializer):
        chat = models.Chat.objects.create(chat_type=models.ChatType.private.name)
        try:
            models.DirectChat = serializer.save(from_user=self.request.user, chat=chat)
        except IntegrityError:
            raise ValidationError({'detail': f'Дубликат приватного чата'})
        
