from channels.db import database_sync_to_async
from django.contrib.auth import get_user_model
from .exceptions import ClientError
from .models import Chat, Message, ChatType
User = get_user_model()



@database_sync_to_async
def get_rooms_to_send(chat):
    
    chat = Chat.objects.get(id=chat)
    if chat.chat_type == ChatType.private.value:
        send_to_rooms = [chat.direct.from_user.room, chat.direct.to_user.room]
    if chat.chat_type == ChatType.public.value:
        send_to_rooms = list(chat.group.users.all().values_list('room', flat=True))
    if chat.chat_type == ChatType.channel.value:
        send_to_rooms = list(chat.channel.users.all().values_list('room', flat=True))
    return send_to_rooms


@database_sync_to_async
def get_room_or_error(room_id, user):
    """
    Tries to fetch a room for the user, checking permissions along the way.
    """
    # Check if the user is logged in
    if not user.is_authenticated:
        raise ClientError("USER_HAS_TO_LOGIN")
    # Find the room they requested (by ID)
    try:
        room = Chat.objects.get(hash=room_id)
    except Chat.DoesNotExist:
        raise ClientError("ROOM_INVALID")
    return room


@database_sync_to_async
def get_group_list(user):
    # Check if the user is logged in
    if not user.is_authenticated:
        raise ClientError("USER_HAS_TO_LOGIN")

    rooms = user.rooms.all()
    room_list = [i.hash for i in rooms]
    return set(room_list)


@database_sync_to_async
def save_message(author_id, chat, text):
    chat = Chat.objects.get(id=chat)
    return Message.objects.create(author_id=author_id, chat=chat, text=text)

@database_sync_to_async
def delete_message(id):
    try:
        msg = Message.objects.get(id=id)
        msg.delete()
        return 204
    except Message.DoesNotExist:
        return 404
    

@database_sync_to_async
def create_room(name, user):
    room = Chat.objects.create(name=name)
    room.users.add(user)
    return room