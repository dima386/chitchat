from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

urlpatterns = [
    path('api/auth/', include('chatauth.urls')),
    path('api/auth/', include('djoser.urls')),
    path('api/auth/', include('djoser.urls.jwt')),
    path('api/chat/', include('chitchat.urls')),
    path('admin/', admin.site.urls),
] + static('media/', document_root=settings.MEDIA_ROOT
) + static('static/', document_root=settings.STATIC_ROOT
)
