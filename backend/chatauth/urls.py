from . import views
from rest_framework import routers
from django.urls import path
from django.urls import re_path
router = routers.DefaultRouter()
from . import views

router.register(r'user-search', views.UserSearchViewSet, basename='user-search')


urlpatterns = [
    re_path(r"^jwt/create/?", views.LoginView.as_view(), name="jwt-create"),
    re_path(r"^logout/?", views.LogoutView.as_view(), name="logout"),
    re_path(r"^profile/?", views.ProfileViewSet.as_view(), name="profile"),
] + router.urls
