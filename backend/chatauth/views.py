from django.contrib.auth import get_user_model
from django.contrib.auth import login, logout
from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.views import APIView
from .serializers import UserListSerializer, UserProfileSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.filters import SearchFilter
User = get_user_model()


class LoginView(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        login(request, serializer.user)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class LogoutView(APIView):
    def post(self, request, *args, **kwargs):
        logout(request)
        return Response({'status':'ok'}, status=status.HTTP_200_OK)

from django.db.models import Q, Count

class UserSearchViewSet(ReadOnlyModelViewSet):
    serializer_class = UserListSerializer
    filter_backends = [SearchFilter]
    search_fields = ['username', 'last_name', 'first_name']
    
    def get_queryset(self):
        user = self.request.user
        return User.objects.annotate(
            has_peer = Count('peers', filter=Q(peers=user), distinct=True), 
        ).exclude(id=user.id)


class ProfileViewSet(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserProfileSerializer

    def get_object(self):
        self.kwargs[self.lookup_field] = self.request.user.pk
        return super().get_object()
