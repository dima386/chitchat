from django.contrib.auth.models import AbstractUser
from django.db import models
import uuid
from core.image_crop import path_and_rename, Translate, ImageConvert


def uuid4_hex():
    return uuid.uuid4().hex


class User(AbstractUser):
    room = models.CharField(max_length=256, default=uuid4_hex)
    logo = models.ImageField('изображение', upload_to='user/avatar', blank=True, null=True)
    peers = models.ManyToManyField('self', through='chitchat.DirectChat')

    def save(self, *args, **kwargs):
        try:
            self.logo = ImageConvert().resize_xs(self.logo, self.logo.name)
        except FileNotFoundError:
            self.logo = None
        super(User, self).save(*args, **kwargs) 