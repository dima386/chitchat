from rest_framework import serializers
from django.contrib.auth import get_user_model
User = get_user_model()


class UserListSerializer(serializers.ModelSerializer):
    has_peer = serializers.SerializerMethodField()

    class Meta:
        model = User
        read_only_fields = ('username',)
        fields = ('id', 'username', 'last_name', 'first_name', 'email' , 'room', 'logo', 'has_peer')
    
    def get_has_peer(self, obj):
        return bool(getattr(obj, 'has_peer', False))


class UserCurrentSerializer(serializers.ModelSerializer):
    '''
    Замена для стандартного сериалайзера djoser "current_user"
    '''
    class Meta:
        model = User
        fields = ('id', 'username', 'last_name', 'first_name', 'logo', 'email', 'room')


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('last_name', 'first_name', 'logo', 'email')
