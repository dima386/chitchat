from django.core.management.base import BaseCommand
from chitchat.models import ChannelChat, DirectChat, GroupChat, Chat, UserChannelChat, UserGroupChat, UserChat, ChatType
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(BaseCommand):

    def handle(self, *args, **options):
        chats = Chat.objects.all()
        for chat in chats:
            if chat.chat_type == ChatType.private.name:
                users = User.objects.filter(id__in=chat.users.values_list('id', flat=True))
                if len(users) == 2:
                    DirectChat.objects.get_or_create(chat=chat, from_user=users[0], to_user=users[1])
            if chat.chat_type == ChatType.public.name:
                group, _ = GroupChat.objects.get_or_create(chat=chat, name=chat.name, logo=chat.logo, owner=chat.owner)
                users = User.objects.filter(id__in=chat.users.values_list('id', flat=True))
                group.users.set(users)

            if chat.chat_type == ChatType.channel.name:
                print(ChatType.channel.name)
                # group, _ = ChannelChat.objects.get_or_create(chat=chat, name=chat.name, logo=chat.logo, owner=chat.owner)
                # users = chat.users.all()
                # group.users.set(users)